# README #

## AMORE System Monitor
* The latest version was committed on 08-22-2018
* CURRENT FILE STRUCTURE
    + amore-system-health/
        + .config/
            + ride_login
        + doc/
        + output/(ignored)
        + resources/
            + requirements.txt
        + src/
            + tools/
                + \_\_init\_\_.py
                + api_alive
                + logger.py
                + retry.py
                + mail.py
            + modules/
                + \_\_init\_\_.py
                + ride_login
            + metropia_monitor.py
        + README.md
        + .gitignore
* This is the System Status and Functionalities monitor, the modules it monitors include:
    1. 
    
## How to Setup
* The files you will need to run the monitor
    + config/
    + src/
* Setup
    + Please have a look at requirements.txt. If any of the libaries are missing on your computer, please run "pip install -r requirements.txt"
* Configuration
    + IMPORTANT: for each module, there is a config.ini file, it is for setting up required parameters to run the monitor. Please only modify it when necessary.
    + If you want get email notification when there is a test fail, please go to the config file for the module you want to monitor, and add your email to the email recipients section. Please strictly follow the adding rule. 
* Run the monitor
    + Run it using command line "python amore_monitor.py production/sandbox/dev1"
        - Command to run it to test production: "python amore_monitor.py production"
        - Command to run it to test sandbox: "python amore_monitor.py sandbox"

## Tests Result
* All test results will be saved in the folder /log/all/
* All test results with one or more failed cases will also be saved to /log/fail/

## Contribution guidelines
* Please clone the remote repo to your local device and create your own branch before you start working on the code
* When you finish your work, commit your final change with a message starting with [In Review] to your branch and let the repo admin know
* If there is no issue during the code review, your changes shall be merged to the master branch

## Who do I talk to?
* With any questions, please contact @zhaohan93