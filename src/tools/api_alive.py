import urllib2
import os
import datetime

import retry
import logger


@retry.retry(Exception, tries=2, delay=2, logger=logger.logger)
def api_check(result, api):
    try:
        if urllib2.urlopen(api).code is 200:
            result = '[passed] (%s) \n' % (api)
        else:
            result = '[failed]: (%s) is DOWN\n' % (api)
    except urllib2.HTTPError, e:
        result = '[failed] %s when loading (%s)\n' % (e, api)

    return result
