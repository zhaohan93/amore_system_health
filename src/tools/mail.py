import smtplib
import os
try:
    import configparser
except:
    from six.moves import configparser
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import logger


def get_config():
    config = configparser.ConfigParser()
    config_file_dir =  os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), 'config')
    config_file_dir =  os.path.join(config_file_dir, 'mail', 'config.ini')

    try:
        config.read(config_file_dir)
        return config._sections
    except:
        logger.logger.info('config for mail is empty')
        return {}


def send_email(recipients=['han.zhao@metropia.com'], message='Empty message, please check', subject='[Metropia System Monitor] Empty message, please check'):

    from_addr = get_config()['sender']['email']
    password = get_config()['sender']['password']
    smtpserver = get_config()['server']['smtp_server_address'] + ':' + get_config()['server']['port']

    # check if the message has html
    if '</html>' in message:
        html_message = message.split('</html>')[0] + '</html>'
        plain_message = message.split('</html>')[1]
        message = MIMEMultipart('alternative')
        message['Subject'] = subject
        message['From'] = from_addr
        message['To'] = str(recipients)
        msg_part1 = MIMEText(plain_message, 'plain')
        msg_part2 = MIMEText(html_message, 'html')
        message.attach(msg_part1)
        message.attach(msg_part2)
        message = message.as_string()
    else:
        header = 'From: %s\n' % from_addr
        header += 'To: %s\n' % recipients
        header += 'Subject: %s\n\n' % subject
        message = header + message

    error = ""
    try:
        server = smtplib.SMTP(smtpserver)
        server.ehlo()
        server.starttls()
        server.ehlo()
        server.login(from_addr, password)
        # server.set_debuglevel(1)
        server.sendmail(from_addr, recipients, message)
        logger.logger.info('e-mail sent to:\n\t\t\t\t' + str(recipients) + '\n')
        server.quit()

    except Exception as e:
        error = e
        logger.logger.info('Send mail error')
        logger.logger.info(error)

    return error
