import MySQLdb
import _mysql_exceptions
import datetime
import sys

import logger

''' # What Mario Provided for Han
host = '192.168.1.95'
port = 3306
user_name = 'han'
my_password = 'han1234'
database_name = 'ops'
TABLE_NAME = 'han'
'''

host = '54.187.109.213'
port = 3306
user_name = 'han'
my_password = '1234'
database_name = 'Metropia_Monitor'


def excute(db, sql):
    try:
        cursor = db.cursor()
        cursor.execute(sql)
        db.commit()
    except _mysql_exceptions.OperationalError:
        pass
    except _mysql_exceptions.IntegrityError:
        pass
    except _mysql_exceptions.ProgrammingError as err:
        print "ProgrammingError: \n%s\nwhen executing (%s) for monitor testing database at (%s)" % (err, sql, host)
    except:
        logger.logger.error('Error (%s) executing (%s) for monitor testing database at (%s)' % (sys.exc_info()[0], sql, host))


def write_result_to_db(time, module_names, results, error_msgs):
    # results = 0 --> PASS
    # results = 1 --> FAIL as Mario suggested
    time = datetime.datetime.utcfromtimestamp(int(time)).strftime('%Y-%m-%d %H:%M:%S')
    print time
    for i, module in enumerate(module_names):
        try:
            db = MySQLdb.connect(host=host, port=port, user=user_name, passwd=my_password, db=database_name)

            sql = "CREATE TABLE {tn} (date datetime PRIMARY KEY)".format(tn=module)
            excute(db, sql)

            sql = "ALTER TABLE {tn} ADD result INT(2), ADD error_msg TEXT DEFAULT NULL".format(tn=module)
            try:
                excute(db, sql)
            except:
                pass

            sql = "INSERT INTO {tn} (date, result, error_msg) VALUES ('{time}', {results}, '{error_msg}')".\
                  format(tn=module, time=time, results= 0 if results[i] else 1, error_msg=error_msgs[i])
            excute(db, sql)

            db.close()
        except:
            logger.logger.error('connect database for saving monitor testing result failed (%s)' % host)
