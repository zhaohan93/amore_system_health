import sqlite3
import os

import logger


FILE_DIR = os.path.dirname(os.path.abspath(__file__))
TABLE_NAME = 'metropia_system_monitor'
DB_FILE_DIR = os.path.join(os.path.dirname(os.path.dirname(FILE_DIR)), 'doc', 'local_db')
if not os.path.exists(DB_FILE_DIR):
    os.mkdir(DB_FILE_DIR)
DB_FILE = os.path.join(DB_FILE_DIR, 'DB_b0fde4a1c2e30730f8cc0568d7349a9d5a5fc823')


def create_db(module_name):

    if not os.path.isfile(DB_FILE):
        conn = sqlite3.connect(DB_FILE)
        c = conn.cursor()
        c.execute('CREATE TABLE {tn} (date real PRIMARY KEY)'.format(tn=TABLE_NAME))
    else:
        conn = sqlite3.connect(DB_FILE)
        c = conn.cursor()
    try:
        c.execute("ALTER TABLE {tn} ADD COLUMN '{cn}' {ct} DEFAULT 'None'".format(tn=TABLE_NAME, cn=module_name, ct='INTEGER'))
    except:
        pass
    conn.commit()
    conn.close()


def write_result_to_db(time, module_names, results):
    # results = 1 --> PASS
    # results = 0 --> FAIL
    update_query = ''
    for i, module in enumerate(module_names):
        update_query += '{cn2}={results},'.format(cn2=module, results= 1 if results[i] else 0)
    update_query = update_query[:len(update_query)-1]

    conn = sqlite3.connect(DB_FILE)
    c = conn.cursor()

    try:
        c.execute("INSERT OR IGNORE INTO {tn} ('date') VALUES ({time})"\
                  .format(tn=TABLE_NAME, time=time))
    except:
        logger.logger.warning('time: ' + str(time) + ' has already been inserted')

    c.execute("UPDATE {tn} SET {query} WHERE {cn1}=({time})".\
              format(tn=TABLE_NAME, query=update_query, cn1='date', time=time))

    conn.commit()
    conn.close()


def db_analysis(modules, timestamp_start, timestamp_end, localnow_date):
    if not os.path.isfile(DB_FILE):
        return 'Couldn\'t find the local database file'
    else:
        conn = sqlite3.connect(DB_FILE)
        c = conn.cursor()

        html_result = '<html><head><style type="text/css">'\
                 '.result_table {border-collapse:collapse;border-spacing:0;}'\
                 '.result_table td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}'\
                 '.result_table th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}'\
                 '.result_table .result_table-failed{background-color:red;color:#000000;text-align:center;vertical-align:top}'\
                 '.result_table .result_table-passed{background-color:green;text-align:center;vertical-align:top}'\
                 '.result_table .result_table-cell{text-align:center;vertical-align:top}'\
                 '.result_table .result_table-title{font-weight:bold;text-align:center;vertical-align:top}'\
                 '</style></head><body>'
        table_result = []
        line1 = ''
        line2 = ''
        line3 = ''
        line4 = ''
        line5 = ''
        title = '%s Monitor Summary' % (localnow_date)
        html_result += '<h2>' + title + '</h2>'
        result = title + '\n'
        for module in modules:
            module_name = module.__name__.split('.')[1]
            result += '[%s]\n' % (module_name)

            total = 0
            failed = 0
            for row in c.execute("SELECT {cn} FROM {tn} where date BETWEEN {st} AND {et}".\
                                 format(cn=module_name, tn=TABLE_NAME, st=timestamp_start, et=timestamp_end)):
                if row[0] != 'None':
                    total += 1
                if row[0] == 0:
                    failed += 1
            pass_rate = str((total-failed)*1.0/total*100)[:5]

            if failed:
                result += '\ttotal tested %s times\n\tFAILED %s times\n\tpassed rate %s%%\n' % (str(total), str(failed), pass_rate)
            else:
                result += '\ttotal tested %s times\n\t100%% passed\n' % (str(total))

            table_result.append([module_name, total, failed, pass_rate + '%'])

        for each in table_result:
            line1 += '<th class="result_table-cell">%s</th>' % each[0]
            line2 += '<td class="result_table-cell">%s</td>' % each[1]
            line3 += '<td class="result_table-cell">%s</td>' % each[2]
            if not each[3] == '100.0%':
                line4 += '<td class="result_table-cell">%s mins</td>' % (each[2] * 5)
                line5 += '<td class="result_table-failed">%s</td>' % each[3]
            else:
                line4 += '<td class="result_table-cell">-</td>'
                line5 += '<td class="result_table-passed">%s</td>' % each[3]

        table_result = '<table class="result_table">'\
                       '<tr>'\
                       '<th class="result_table-title">Module</th>'\
                       '{}'\
                       '</tr>'\
                       '<tr>'\
                       '<td class="result_table-title">Total Tested</td>'\
                       '{}'\
                       '</tr>'\
                       '<tr>'\
                       '<td class="result_table-title">Failed</td>'\
                       '{}'\
                       '</tr>'\
                       '<tr>'\
                       '<td class="result_table-title">Approx Downtime</td>'\
                       '{}'\
                       '</tr>'\
                       '<tr>'\
                       '<td class="result_table-title">Pass Rate</td>'\
                       '{}'\
                       '</tr>'\
                       '</table>'.format(line1, line2, line3, line4, line5)
        html_result += table_result + '</body></html>'

        return html_result + '&' + result
