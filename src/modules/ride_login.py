import datetime
import os
import sys
try:
    import configparser
except:
    from six.moves import configparser
import requests
import json

from tools import logger
from tools import api_alive


def get_config():
    config = configparser.ConfigParser()
    config_file_dir =  os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), 'config')
    config_file_dir =  os.path.join(config_file_dir, 'ride_login', 'config.ini')

    try:
        config.read(config_file_dir)
        return config._sections
    except:
        logger.logger.info('config for ride login is empty')
        return {}


class Ride_Login_Check():

    def __init__(self, config, env):
        self.config = config
        self.env = env
        self.result = ''
        self.error = ''

        self.setup()

    def setup(self):

        self.ride_login_url = self.config[self.env]['ride_login_url']
        self.ride_login_header = self.config[self.env]['ride_login_header']
        self.ride_login_body = self.config[self.env]['ride_login_body']

    # Test functions start here
    def check(self):
        logger.logger.info('starting ride login test')
        ride_login_check_response = requests.post(self.ride_login_url, headers=json.loads(self.ride_login_header), data=self.ride_login_body)
        ride_login_check_response = json.loads(ride_login_check_response.text)

        if ride_login_check_response["result"] != 1:
            fail_message = "[failed] Ride Login Check, Fail Message: %s, url: %s\n" % (ride_login_check_response["message"], self.ride_login_url)
            self.result += fail_message
            self.error += fail_message
        else:
            self.result += "[passed] Ride Login Check, url: %s\n" % (self.ride_login_url)


def run_test(config, log, env, now=None):

    newTest = Ride_Login_Check(config, env)

    newTest.check()

    result = newTest.result
    logger.logger.info(result)
    log.write('ride login at %s UTC\n' % datetime.datetime.utcnow() + result + '\n')

    return newTest.error
