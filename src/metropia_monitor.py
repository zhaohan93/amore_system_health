import datetime
import sys
import os
import shutil
import time
try:
    import configparser
except:
    from six.moves import configparser


from modules import ride_login


from tools import mail
from tools import logger
from tools import local_db
from tools import monitor_database

FILE_DIR = os.path.dirname(os.path.abspath(__file__))
OUTPUT_DIR = os.path.join(FILE_DIR, '..', 'output')
if not os.path.exists(OUTPUT_DIR):
    os.mkdir(OUTPUT_DIR)


# Please declare all tests you want to run here
MODULES = [
    ride_login
]

# 0: do not send dailt summary; 1: send daily summary
DAILY_SUMMARY = 0


def get_config():
    config = configparser.ConfigParser()
    config_file_dir =  os.path.join(os.path.dirname(os.path.dirname(__file__)), 'config')
    config_file_dir =  os.path.join(config_file_dir, 'daily_summary_email', 'config.ini')

    try:
        config.read(config_file_dir)
        return config._sections
    except:
        logger.logger.info('config for db analysis recipients is empty')
        return {}


def main():
    if len(sys.argv) < 2:
        sys.exit("Usage: %s production/sandbox" % sys.argv[0])
    if sys.argv[1] != 'production' and sys.argv[1] != 'sandbox':
        sys.exit("Usage: %s production/sandbox" % sys.argv[0])

    # create all and fail folders
    if not os.path.exists(os.path.join(OUTPUT_DIR, 'all')):
        os.makedirs(os.path.join(OUTPUT_DIR, 'all'))
    if not os.path.exists(os.path.join(OUTPUT_DIR, 'fail')):
        os.makedirs(os.path.join(OUTPUT_DIR, 'fail'))

    utcnow = datetime.datetime.utcnow()
    timezone = -7
    localnow = utcnow + datetime.timedelta(hours=timezone)
    localnow_date = localnow.strftime('%Y-%m-%d')
    local_date_in_datetime = datetime.datetime.strptime(localnow_date, '%Y-%m-%d')
    zero_clock_timestamp = (local_date_in_datetime - datetime.datetime(1970,1,1)).total_seconds() - timezone * 3600

    log_file = os.path.join(OUTPUT_DIR, "all", "%s" % utcnow.strftime("%Y-%m-%d-%H-%M-%f.log"))

    # Write to log
    env = sys.argv[1]
    result_flag = True
    fail_count = 0
    with open(log_file, "w") as log:
        log.write('%s test begins at UTC %s\n\n' % (sys.argv[1], utcnow))
        timestamp_now = time.time()

        module_names = list()
        module_results = list()
        module_msgs = list()
        for item in MODULES:
            module_name = item.__name__.split('.')[1]
            module_names.append(module_name)

            # initilize Local db
            local_db.create_db(module_name)

            config = item.get_config()
            error = item.run_test(config, log, env, utcnow)
            if error:
                fail_count += 1
                result_flag = False
                log.write('%s failed\n\n' % (item))
                # send e-mail to recipients set in config file when test failed
                content = error
                subject_contents = "[AMORE System Monitor] Test Failed - %s" % (item)
                recipients = config['email_recipients']['recipients'].split(',')
                #mail.send_email(recipients=recipients, message=content, subject=subject_contents)

                module_results.append(False)
                content = content.replace('\'', '\\\'')
                module_msgs.append(content)
            else:
                module_results.append(True)
                module_msgs.append('N/A')

        duration = (datetime.datetime.utcnow() - utcnow).total_seconds()
        log.write('\n----------------------------------------------------------------------\n')
        if result_flag:
            log.write('%s test passed\n' % sys.argv[1])
        else:
            log.write('%s test failed\n' % sys.argv[1])

        log.write('total fail test cases: %s\n' % fail_count)
        log.write('total duration: %s sec' % duration)
        log.close()

    if 'production test failed' in open(log_file, "r").read():
        shutil.copy(log_file, os.path.join(OUTPUT_DIR, 'fail'))

    if DAILY_SUMMARY:
        # write to local db for daily summary email
        local_db.write_result_to_db(timestamp_now, module_names, module_results)
        # write to online db for analysis
        monitor_database.write_result_to_db(timestamp_now, module_names, module_results, module_msgs)
        
        localnow = datetime.datetime.utcnow() + datetime.timedelta(hours=timezone)
        # if the current local time is between 23:45:00 and 23:59:59
        current_time_in_seconds = int((localnow - local_date_in_datetime).total_seconds()) / 60
        logger.logger.info('local current time in minutes is: ' + str(current_time_in_seconds))
        if current_time_in_seconds > 1425:
            summary_log_dir =  os.path.join(os.path.dirname(os.path.dirname(__file__)), 'config', 'daily_summary_email', 'log.log')
            if not os.path.exists(summary_log_dir):
                summary_log = open(summary_log_dir, "w+")
                summary_log.close()
            with open(summary_log_dir) as summary_log:
                if localnow_date not in summary_log.read():
                    summary_log.close()
                    # get db data for the day
                    timestamp_end = time.time()
                    # for console log
                    start_time = datetime.datetime.utcfromtimestamp(zero_clock_timestamp).strftime('%Y-%m-%d %H:%M')
                    end_time = datetime.datetime.utcfromtimestamp(timestamp_end).strftime('%Y-%m-%d %H:%M')
                    logger.logger.info('Daily Summary: getting data from datebase for the time range: %s UTC to %s UTC' % (start_time, end_time))
                    # run db data analysis
                    daily_summary_content = local_db.db_analysis(MODULES, zero_clock_timestamp, timestamp_end, localnow_date)
                    # get email addresses from config then send emails
                    db_daily_recipients = get_config()['email_recipients']['recipients'].split(',')
                    mail.send_email(recipients=db_daily_recipients, message=daily_summary_content, subject="[Metropia System Monitor] Daily Summary")
                    # make a record to avoid email to be sent again
                    with open(summary_log_dir, "a") as summary_log:
                        summary_log.write(localnow_date + '\n')
                        summary_log.close()
                else:
                    summary_log.close()
                    logger.logger.info('today\'s summary has been sent already')


if __name__ == '__main__':
    main()
